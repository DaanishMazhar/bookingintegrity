import json
import time

from selenium.common.exceptions import WebDriverException
from factory import *


class Response(object):
    pass


def run_scraper(pnr, last_name, data):
    data = json.loads(data)

    response = []

    driver = build_webdriver("firefox")
    driver.implicitly_wait(30)

    driver.get("https://booking.qatarairways.com/nsp/views/manageBooking.action?selLang=en&pnr={}&lastName={}".format(pnr, last_name))

    driver.find_element_by_xpath("//a[@id='manageFlights:seatPrefLink']")

    time.sleep(2)

    driver.execute_script('$("#manageFlights\\\\:seatPrefLink").click();')

    time.sleep(1)

    # Clicking on the expanded flight to make sure they're all collapsed before we start
    expanded_flight = driver.find_element_by_xpath("//button[contains(@id, 'flight')]/parent::h2/parent::div/following-sibling::div[contains(@class, 'collapse') and contains(@class, 'show')]/parent::div[@class='card']//button[1]")
    expanded_flight.click()
    time.sleep(3)

    # Reduces the implicit wait to 1 second
    driver.implicitly_wait(1)

    # Finds all flights
    flights = driver.find_elements_by_xpath("//button[contains(@id, 'flight')]/parent::h2/parent::div/following-sibling::div[contains(@class, 'collapse')]/parent::div[@class='card']")
    for i in range(len(flights)):
        if "No seat map available" in flights[i].text:
            continue

        # Expand the flight
        flight_button = flights[i].find_element_by_xpath(".//button[contains(@id, 'flight')]")
        flight_button.click()

        time.sleep(2)

        driver.implicitly_wait(1)

        seat_map = flights[i].find_element_by_xpath(".//div[@class='sOuterViewport']")

        # Qatar doesn't add the flight number to the seat selection area anymore, so we're using the "Departure - Arrival" information
        flight_number = flight_button.text

        passengers = flights[i].find_elements_by_xpath(".//button[contains(@class,'passDetails')]")

        r = Response()
        for flight_data in data["flights"]:
            # Builds the flight information from the JSON data and check it matches the current expanded flight
            flight = "{} - {}".format(flight_data["departure"], flight_data["arrival"])
            if not flight in flight_number:
                continue

            r.flight = flight_data["number"]
            r.seats = []

            seats = flight_data["seats"]
            time.sleep(1)

            for j in range(len(seats)):
                passengers[j].click()
                time.sleep(2)

                try:
                    # Try finding the desired seat
                    seat_map.find_element_by_xpath(".//button[@data-value='{}' and contains(@class, 'availSeat')]".format(seats[j]))
                    r.seats.append(seats[j])
                except WebDriverException:
                    try:
                        # If the desired seat is not found or is not available, we try getting the current selected seat
                        current_seat = seat_map.find_element_by_xpath(".//button[@class='noAvailSeat selected']").get_attribute("data-value")
                        r.seats.append(current_seat)
                    except:
                        pass
                    finally:
                        continue

                time.sleep(1)

                driver.execute_script('$("button.availSeat[data-value=\'{}\']").click()'.format(seats[j]))
                time.sleep(1)
                driver.find_element_by_xpath("//button[text()='Select this seat']").click()
                time.sleep(1)

            response.append(r)

    time.sleep(2)

    # Increases the implicit wait to 30 seconds
    driver.implicitly_wait(30)

    # Clicks on the Continue button
    driver.find_element_by_xpath("//input[@id='chargeableSeat:seatMapSaveBtn']").click()

    # Looks for the Confirm button and click it
    confirm_button = driver.find_element_by_xpath("//input[@id='manageBookingSeatReview:ConfirmSeats']")
    confirm_button.click()

    time.sleep(5)

    # Looks for the Back link and click it
    back_link = driver.find_element_by_xpath("//a[@id='manageBookingSeatReview:backToManageBooking']")
    back_link.click()

    driver.find_element_by_xpath("//a[@id='manageFlights:seatPrefLink']")

    driver.set_window_size(1920, 9999)
    driver.find_element_by_tag_name("body").screenshot("output.png")

    time.sleep(5)

    driver.quit()

    return json.dumps([o.__dict__ for o in response])
