import json
import time

from selenium.common.exceptions import WebDriverException

from factory import *


class Response(object):
    pass


driver = build_webdriver("chromium")
json_data = None

def run_scraper(first_name, last_name, record_locator, data):
    global json_data
    json_data = json.loads(data)
    driver.implicitly_wait(30)

    response = []

    driver.get("https://www.aa.com/reservation/view/find-your-reservation")

    driver.find_element_by_id("findReservationForm.firstName").send_keys(first_name)
    driver.find_element_by_id("findReservationForm.lastName").send_keys(last_name)
    driver.find_element_by_id("findReservationForm.recordLocator").send_keys(record_locator)
    time.sleep(1)
    driver.find_element_by_id("findReservationForm.submit").click()

    choose_seats = driver.find_element_by_xpath("//a[@id='seatsRedirectUrl']")
    choose_seats.click()

    flights = driver.find_elements_by_xpath("//a[contains(@id,'chooseFlightLink')]")
    for i in range(len(flights)):
        r = Response()

        wait_overlay()

        flights = driver.find_elements_by_xpath("//a[contains(@id,'chooseFlightLink')]")

        flights[i].click()

        wait_overlay()

        seat_map = driver.find_element_by_xpath("//div[@id='jsSeatMapView']")

        flight_number = seat_map.find_element_by_xpath(".//div[@id='flightDetailsLegendContainer']//ul//li[1]").text
        r.flight = flight_number
        r.seats = []

        seats = find_seats(flight_number)

        passengers = seat_map.find_elements_by_xpath(".//ul[@id='passengerList']//button[contains(@id,'passenger')]")

        for passenger in passengers:
            passenger.click()

            time.sleep(2)

            wait_overlay()

            try:
                seat = seats.pop(0)
            except:
                break

            try:
                seat_map.find_element_by_xpath("//div[@role='button' and @data-seatno='{}' and contains(@class,'is-available') and not(contains(@class,'mce')) and not(contains(@class,'preferred'))]".format(seat)).click()
                r.seats.append(seat)
            except WebDriverException:
                current_seat = passenger.find_element_by_xpath(".//div[contains(@class,'seatNumber')]").text
                r.seats.append(current_seat)

        response.append(r)

        time.sleep(3)

        submit_button = seat_map.find_element_by_xpath(".//button[@type='submit' and @id='nextFlightButton']")
        submit_button.click()

    driver.find_element_by_xpath("//a[@id='seatsRedirectUrl']")

    driver.set_window_size(1920, 9999)
    driver.find_element_by_tag_name("body").screenshot("output.png")

    time.sleep(5)

    driver.quit()

    return json.dumps([o.__dict__ for o in response])


def wait_overlay():
    driver.implicitly_wait(1)
    while len(driver.find_elements_by_xpath("//div[@class='aa-busy-module']")) > 0:
        time.sleep(1)
        continue
    driver.implicitly_wait(30)


def find_seats(flight_number):
    return [x['seats'] for x in json_data['flights'] if x['number'] == flight_number][0]