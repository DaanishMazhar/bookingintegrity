import json
import time

from factory import *


def run_scraper(booking_reference, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.britishairways.com/travel/managebooking/public/en_us")

    time.sleep(5)

    data = []

    driver.execute_script('$("#bookingRef", $("#bookingRef").shadowRoot).val("{}")'.format(booking_reference))
    time.sleep(2)
    driver.execute_script('$("#lastname", $("#lastname").shadowRoot).val("{}")'.format(last_name))

    time.sleep(3)

    driver.find_element_by_id("findbookingbuttonsimple").click()

    time.sleep(10)

    design_alert = driver.find_elements_by_class_name('js-opt-out-splash-link')
    if len(design_alert) > 0:
        driver.execute_script('$(".js-opt-out-splash-link").click()')

    checkbox_alert = driver.find_elements_by_id("confirm-contact-input")
    if len(checkbox_alert) > 0:
        driver.execute_script('$("#confirm-contact-input").click()')

    time.sleep(2)

    driver.execute_script('$("input[name=\'Continue\']").click()')

    time.sleep(5)

    driver.find_element_by_class_name("open-all").click()

    time.sleep(1)

    flights = driver.find_elements_by_xpath("//li[contains(@class,'tabHeading')]")
    for flight in flights:
        flight_number = flight.find_elements_by_xpath(".//a[contains(@id,'flightAccordion')]")

        if len(flight_number) == 0:
            continue

        flight_number = flight_number[0].text.split()[0]

        seats = flight.find_element_by_class_name('allocation').text.split(", ")

        for seat in seats:
            data.append({
                "seat": seat,
                "flight": flight_number
            })

    driver.quit()

    return json.dumps(data)
