import json
import time

from factory import *


def run_scraper(pnr, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://booking.qatarairways.com/nsp/views/manageBooking.action?selLang=en&pnr={}&lastName={}".format(pnr, last_name))

    time.sleep(15)

    rows = driver.find_elements_by_xpath("//div[contains(@class, 'table-content')]")

    scraped_data = []
    for row in rows:
        data = row.find_element_by_xpath("(.//div[contains(@class,'name-width-no')])/div/div").text
        data = data.split("E-ticket")

        passenger = data[0].split("Passenger name")[1].strip()
        eticket = data[1].strip()

        class_type = row.find_element_by_xpath("(.//div[contains(@class,'cabin-brd-right')])/div").text
        class_type = class_type.split("Class")[1].strip()

        flights = row.find_elements_by_xpath("(./div[3])//a[@title='Flight Number']")
        seats = row.find_elements_by_xpath("(.//em[contains(.,'Seat')])/following-sibling::div")

        data = []
        for i in range(len(seats)):
            if seats[i].text.strip() == "-":
                continue

            data.append({
                seats[i].text: {
                    "flight": flights[i].text,
                    "passenger": passenger,
                    "eticket": eticket,
                    "class": class_type,
                }
            })

        scraped_data.append(data)

    driver.quit()

    return json.dumps(scraped_data)
