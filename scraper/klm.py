import json
import time

from factory import *


def run_scraper(booking_code, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.klm.kz/en/trip")

    time.sleep(7)

    alert_button = driver.find_elements_by_xpath("//button[@id='agreeButton']")
    if len(alert_button) > 0:
        alert_button[0].click()

    time.sleep(1)

    driver.find_element_by_name("bookingCode").send_keys(booking_code)

    time.sleep(2)

    driver.find_element_by_name("lastName").send_keys(last_name)

    time.sleep(1)

    driver.find_element_by_css_selector("div.bw-trip-entry-form__actions button").click()

    time.sleep(15)

    data = []

    flights = driver.find_elements_by_css_selector("div.mat-tab-label")

    for flight in flights:
        flight.click()

        time.sleep(2)

        flight_number = driver.find_element_by_css_selector("div.bwc-flight-segment__operatedBy span").text.split(", ")[1]

        seats = driver.find_element_by_css_selector("span.bw-trip-segment__seat-detail--item-space").text.split(", ")
        for seat in seats:
            data.append({
                "flight": flight_number,
                "seat": seat
            })

    driver.quit()

    return json.dumps(data)