import json
import time

from factory import *


def run_scraper(first_name, last_name, record_locator):
    driver = build_webdriver("chromium")

    driver.get("https://www.aa.com/reservation/view/find-your-reservation")

    driver.find_element_by_id("findReservationForm.firstName").send_keys(first_name)
    driver.find_element_by_id("findReservationForm.lastName").send_keys(last_name)
    driver.find_element_by_id("findReservationForm.recordLocator").send_keys(record_locator)
    driver.find_element_by_id("findReservationForm.submit").click()

    time.sleep(15)

    data = []

    flights = driver.find_elements_by_xpath("//tbody[contains(@id, 'flightCardLegRegular')]")
    for flight in flights:
        flight_number = flight.find_element_by_xpath(".//span[@class='flightCardFlightNumber']").text

        seats = flight.find_elements_by_xpath(".//ul[contains(@class, 'flightCardSeatsDisplay')]//li")

        for seat in seats:
            seat = seat.find_element_by_xpath(".//a").text.split("\n")[0]

            if seat == "Choose seats":
                continue

            data.append({
                "flight": flight_number,
                "seat": seat,
            })

    driver.quit()

    return json.dumps(data)
