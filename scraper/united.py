import json
import time

from factory import *


def run_scraper(confirmation_number, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.united.com/en/us/manageres/mytrips")

    driver.find_element_by_id(".confirmationNumber").send_keys(confirmation_number)
    driver.find_element_by_id(".lastName").send_keys(last_name)
    driver.find_element_by_css_selector("form button[type='submit']").click()

    time.sleep(10)

    data = []

    flights = driver.find_elements_by_xpath("//div[contains(@class, 'app-containers-TripDetails-SingleFlight-singleFlight__gridParent')]")
    for flight in flights:
        seat = flight.find_element_by_xpath(".//span[text()='Seats']/parent::td/following-sibling::td//a").text
        flight_number = flight.find_element_by_xpath(".//span[text()='Flight']/parent::td/following-sibling::td").text

        data.append({
            "flight": flight_number,
            "seat": seat,
        })

    driver.quit()

    return json.dumps(data)
