import json
import time

from selenium.webdriver import ActionChains

from factory import *


def run_scraper(booking_reference, last_name):
    driver = build_webdriver("firefox")

    driver.get("https://www.brusselsairlines.com/com/practical-information/manage-booking/default.aspx")

    time.sleep(1)
    driver.find_element_by_id("contentMain_txtLastName").send_keys(last_name)

    time.sleep(2)
    driver.find_element_by_id("contentMain_txtPnr").send_keys(booking_reference)

    time.sleep(1)
    driver.find_element_by_id("contentMain_lkbGo").click()

    time.sleep(20)

    data = []

    expanders = driver.find_elements_by_xpath("//div[@class='flightSummary']//div[@class='expander' and @aria-expanded='false']")
    for expander in expanders:
        time.sleep(1)
        try:
            expander.click()
        except:
            continue

    flights = driver.find_elements_by_xpath("//div[contains(@id,'mmb-flight-item')]")
    for flight in flights:
        flight_number = 0
        seats = 0

        try:
            flight_number = flight.find_element_by_xpath(".//span[@class='flightNumber']/strong").text
            seats = flight.find_elements_by_xpath(".//div[contains(@class,'seats')]")
        except:
            flight.find_element_by_xpath("//div[@class='expander' and @aria-expanded='true']").click()

            time.sleep(1)

            flight_number = flight.find_element_by_xpath(".//span[@class='flightNumber']/strong").text
            seats = flight.find_elements_by_xpath(".//div[contains(@class,'seats')]")

        for seat in seats:
            seat_number = 0;
            try:
                seat_number = seat.find_element_by_xpath('.//p/strong').text
            except:
                continue

            if seat_number == "":
                continue

            data.append({
                "flight": flight_number,
                "seat": seat_number
            })

    driver.quit()

    return json.dumps(data)
