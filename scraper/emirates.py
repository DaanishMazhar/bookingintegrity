import json
import time

from factory import *


def run_scraper(booking_reference, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.emirates.com/english/manage-booking/")

    time.sleep(5)

    driver.find_element_by_id("retrieve-booking-widget-form-lastname").send_keys(last_name)

    time.sleep(2)

    driver.find_element_by_id("retrieve-booking-widget-form-reference").send_keys(booking_reference)

    time.sleep(3)

    driver.find_element_by_xpath("//button[text()='Retrieve Booking']").click()

    data = []

    time.sleep(15)

    driver.find_element_by_id("liTabPassenger").click()

    time.sleep(1)

    passengers = driver.find_elements_by_xpath("//li[contains(@data-ts-tab-name,'passenger')]")
    for passenger in passengers:
        passenger.click()

        time.sleep(1)

        flights = driver.find_elements_by_xpath("//tr[contains(@id,'paxContainer')]")

        for flight in flights:
            seat_element = flight.find_element_by_xpath(".//td[@class='ts-seats']//div[@class='ts-row-2']").text.split(" / ")
            if len(seat_element) == 0:
                continue

            seat = seat_element[0]

            if seat == "":
                continue

            flight_number = flight.find_element_by_class_name("ts-flight-number").text

            data.append({
                "seat": seat,
                "flight": flight_number
            })

    driver.quit()

    return json.dumps(data)