import json
import time

from factory import *


def run_scraper(pnr, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.thaiairways.com/en/manage/my_booking.page")

    time.sleep(1)
    driver.find_element_by_id("pnrCode").send_keys(pnr)

    time.sleep(2)
    driver.find_element_by_id("lastName").send_keys(last_name)

    time.sleep(1)
    driver.execute_script("$('button.bt').click()");

    time.sleep(2)

    if len(driver.window_handles) == 1:
        raise Exception("Could not open details window.")

    time.sleep(5)

    driver.switch_to.window(driver.window_handles[1])

    time.sleep(45)

    data = []

    flights = driver.find_elements_by_xpath("//span[@class='flight-info-airline-flight']//bdo")
    seats = driver.find_elements_by_xpath("//div[@class='servicesbreakdown-segment-container']")

    for i in range(len(flights)):
        flight_number = flights[i].text.replace("(", "").replace(")", "")

        seat_list = seats[i].find_elements_by_xpath(".//bdo")
        for seat_item in seat_list:
            seat_number = seat_item.text.replace("(", "").replace(")", "")

            data.append({
                "flight": flight_number,
                "seat": seat_number
            })


    driver.quit()

    return json.dumps(data)
