import json
import time

from factory import *

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def run_scraper(pnr, last_name):
    driver = build_webdriver("chromium")

    driver.get("https://www.turkishairlines.com/en-us/flights/manage-booking/index.html#page_wrapper")

    time.sleep(5)

    driver.execute_script('$("#ticketNo", $("#ticketNo").shadowRoot).val("{}")'.format(pnr))
    time.sleep(2)
    driver.execute_script('$("#surname", $("#surname").shadowRoot).val("{}")'.format(last_name))

    time.sleep(1)
    driver.find_element_by_id("ticketNo").click()
    time.sleep(1)
    driver.find_element_by_id("ticketNo").clear()
    time.sleep(1)
    driver.find_element_by_id("ticketNo").send_keys(pnr)
    time.sleep(1)

    actions = ActionChains(driver)
    actions.send_keys(Keys.TAB * 2)
    actions.perform()

    time.sleep(3)

    driver.find_element_by_name("submit").click()

    time.sleep(10)

    detail_links = driver.find_elements_by_xpath("//a[contains(@class, 'itinerary-details-btn')]")
    for link in detail_links:
        link.click()
        time.sleep(1)

    flights = driver.find_elements_by_xpath("//span[@data-bind='text : flightNumber']")

    driver.find_element_by_id("selectSeatTab").click()

    time.sleep(1)

    seats = driver.find_elements_by_xpath("//span[contains(@data-bind, 'seatNumber')]")

    data = []

    for i in range(len(flights)):
        data.append({
            "seat": seats[i].text,
            "flight": flights[i].text
        })

    driver.quit()

    return json.dumps(data)
