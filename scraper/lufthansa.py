import json
import time

from factory import *


def run_scraper(booking_code, last_name):
    driver = build_webdriver("firefox")
    driver.delete_all_cookies()

    driver.get("https://www.lufthansa.com/de/en/login?deeplinkRedirect=true#")

    time.sleep(5)

    alert_button = driver.find_elements_by_xpath("//button[@id='cm-selectSpecific']")
    if len(alert_button) > 0:
        alert_button[0].click()

    time.sleep(3)

    driver.find_element_by_xpath("//input[contains(@id,'LoginPNRFormQuery.j_lastname')]").send_keys(last_name)

    time.sleep(2)
    driver.find_element_by_xpath("//input[contains(@id,'LoginPNRFormQuery.j_bookingcode')]").send_keys(booking_code)

    time.sleep(4)
    driver.find_element_by_css_selector("button.btn-primary[type='submit']").click()

    time.sleep(20)
    driver.find_element_by_xpath("//span[text()='Seat details']/parent::a").click()

    time.sleep(25)

    data = []

    flights = driver.find_elements_by_xpath("//div[@class='table-flight-details']//span[contains(@class,'flightData')]")
    seats = driver.find_elements_by_xpath("//div[@id='irc-seat-reservations-panel']//div[@class='grid']")
    for i in range(len(flights)):
        flight_number = flights[i].find_element_by_xpath(".//h5[contains(@class,'flightNumber')]").text
        seat_numbers = seats[i].find_elements_by_xpath(".//strong[@class='seat-num']")

        for seat_number in seat_numbers:
            if seat_number.text == "---":
                continue

            data.append({
                "flight": flight_number,
                "seat": seat_number.text,
            })

    driver.quit()

    return json.dumps(data)
