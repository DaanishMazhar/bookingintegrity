from bot.qatar import run_scraper

json = """{
  "flights": [
    {
      "number": "QR 1086",
      "seats": [
        "99A",
        "25E"
      ]
    },
    {
      "number": "QR 1091",
      "seats": [
        "25A",
        "16A"
      ]
    }
  ]
}"""

print(run_scraper(pnr="URMM77", last_name="MAZHAR", data=json))