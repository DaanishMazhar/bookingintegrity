import os
import random
import sys

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

BOOKING_INTEGRITY_ROOT = os.path.dirname(os.path.abspath(__file__))
BINARIES = {
    "darwin": {
        "chromium": {
            "webdriver": os.path.join(BOOKING_INTEGRITY_ROOT, "bin/macos/chromedriver"),
            "browser": os.path.join(BOOKING_INTEGRITY_ROOT, "bin/macos/Chromium.app")
        },
        "firefox": {
            "webdriver": os.path.join(BOOKING_INTEGRITY_ROOT, "bin/macos/geckodriver"),
            "browser": os.path.join(BOOKING_INTEGRITY_ROOT, "bin/macos/Firefox.app/Contents/MacOS/firefox-bin")
        }
    },
    "linux": {
        "chromium": {
            "webdriver": "/usr/bin/chromedriver",
            "browser": "/usr/bin/chrome"
        },
        "firefox": {
            "webdriver": "/usr/bin/geckodriver",
            "browser": "/usr/bin/firefox"
        }
    }
}

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def build_webdriver(browser):
    if browser == "firefox":
        return build_geckodriver()
    elif browser == "chromium":
        return build_chromedriver()


def build_geckodriver(geckodriver_path=None, firefox_options=None, options=None):
    path = get_binaries("firefox")

    if firefox_options is None:
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.arguments.clear()
        #firefox_options.add_argument("--start-maximized")
        #firefox_options.add_argument('--user-agent="{}"'.format(get_user_agent()))

    firefox_binary = FirefoxBinary(path["browser"])

    if geckodriver_path is None:
        geckodriver_path = path["webdriver"]

    #driver = webdriver.Firefox(executable_path=geckodriver_path, firefox_options=firefox_options, options=options, service_args=["--verbose", "--log-path=./geckodriver.log"])
    driver = webdriver.Firefox(executable_path=geckodriver_path, firefox_binary=firefox_binary)
    #driver.set_window_size(1440 + random.randint(0, 50), 900 + random.randint(0, 50))
    #driver.set_window_position(random.randint(0, 10), random.randint(0, 10))

    return driver


def build_chromedriver(chromedriver_path=None, chrome_options=None, options=None):
    path = get_binaries("chromium")

    if chrome_options is None:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.arguments.clear()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--profile-directory=Default')
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-plugins-discovery")
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument('--user-agent="{}"'.format(get_user_agent()))

        chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
        chrome_options.add_experimental_option('useAutomationExtension', False)

    if options is None:
        options = Options()
        options.binary_location = path["browser"]

    if chromedriver_path is None:
        chromedriver_path = path["webdriver"]

    driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options, options=options, service_args=["--verbose", "--log-path=./chromedriver.log"])
    driver.set_window_size(1440 + random.randint(0, 50), 900 + random.randint(0, 50))
    driver.set_window_position(random.randint(0, 10), random.randint(0, 10))

    return driver


def get_binaries(browser):
    path = {
        "webdriver": None,
        "browser": None
    }

    if sys.platform == 'darwin':
        path["webdriver"] = BINARIES['darwin'][browser]['webdriver']
        path["browser"] = BINARIES['darwin'][browser]['browser']
    elif sys.platform == 'linux' or sys.platform == 'linux2':
        path["webdriver"] = BINARIES['linux'][browser]['webdriver'] #"/usr/bin/chromedriver"
        path["browser"] = BINARIES['linux'][browser]['browser'] #"/usr/bin/chrome"
    else:
        raise NotImplementedError('Operating System "{}" not supported.'.format(sys.platform))

    return path


def get_user_agent():
    ua_list = [
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15",
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36"
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:72.0) Gecko/20100101 Firefox/72.0"
    ]

    return random.choice(ua_list)

